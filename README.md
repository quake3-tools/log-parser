# Quake 3 Log Parser

This library is a basic node module providing the capability to parse Quake 3 log files.

## Usage

Simple example to parse a log file:

```typescript
import { LogFile } from 'q3-log-parser';

var q3log = new LogFile();
q3log.parseLog(gamesLog, function (log) {
    q3log = log;
});
```

If you needs server wide stats you can parse a log and create a instace of `ServerStats` object.

```typescript
import { LogFile, ServerStats } from 'q3-log-parser';

var q3log = new LogFile();
q3log.parseLog(gamesLog, function (log) {
    q3log = log;
    var serverStats = new ServerStats(q3log);
    serverStats.exportToJson('/my/path/to/stats.json');
});
```