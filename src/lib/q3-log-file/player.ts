/** Player */
export class Player {
    public name: string;
    public kills: number;
    public deaths: number;
    public kdratio: number;
    public clientId: number;

    constructor() {
        this.name = '';
        this.kills = 0;
        this.deaths = 0;
        this.kdratio = 0;
        this.clientId = -1;
    }

    public updateStats() {
        if (this.kills > 0 && this.deaths > 0) {
            this.kdratio = parseFloat((this.kills / this.deaths).toFixed(2));
        }
    }

    public clone(player: Player) {
        this.kills = player.kills;
        this.deaths = player.deaths;
        this.name = player.name;
        this.kdratio = player.kdratio;
    }
}
