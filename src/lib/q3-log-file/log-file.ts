import * as fs from 'fs';
import * as readline from 'readline'; 
import { LogPattern } from '../enum/log-pattern.enum';
import { Game } from './game';
import { GameType } from '../enum/game-type.enum';

/** Quake3 Log */
export class LogFile {
    public games: Array<Game>;

    constructor() {
        this.games = new Array<Game>();
    }

    public exportToJson(file: string) {
        var stream = fs.createWriteStream(file);
        stream.write(JSON.stringify(this));
        stream.close();
        stream.on('close', function () {
            console.log('File ' + stream.path + ' saved.');
        });
    }

    public parseLog(file: string, done: (log: LogFile) => void, outfile?: string) {
        var log: LogFile = new LogFile();
        var newGame: Game;
    
        // read file by line
        var reader = readline.createInterface({
            input: fs.createReadStream(file)
        });
    
        // for each line callback parse log line
        reader.on('line', function (line: string) {
            if (line.indexOf(LogPattern.GameStart) > -1) {
                // if game is defined add it to the list of games if a new game was initialized
                if (newGame) {
                    log.games.push(newGame);
                }
    
                // now start from scratch with the new game
                newGame = new Game();
                var gameTypeStr = 'g_gametype\\';
                if (line.indexOf(gameTypeStr) > -1) {
                    var gameType = line.charAt(line.indexOf(gameTypeStr) + gameTypeStr.length);
                    switch (gameType) {
                        case '0':
                            newGame.type = GameType.FreeForAll;
                            break;
                        case '3':
                            newGame.type = GameType.TeamDeathmatch;
                            break;
                        case '4':
                            newGame.type = GameType.CaptureTheFlag;
                            break;
                    }
                }
            }
    
            // parse chat for each game
            var chatPatternIndex = line.indexOf(LogPattern.Chat);
            if (chatPatternIndex > -1) {
                var chatLine = line.substr(chatPatternIndex + LogPattern.Chat.length);
                newGame.chat.push(chatLine);
            }
    
            // parse kills
            var killIndex = line.indexOf(LogPattern.Kill);
            var killIndexPos = killIndex + LogPattern.Kill.length;
            if (killIndex > -1) {
                var lineSubstr = line.substr(killIndexPos);
                var killStat = line.substr(killIndexPos, lineSubstr.indexOf(':')).split(' ');
                
                var killer = newGame.getPlayerById(parseInt(killStat[0]));
                var killed = newGame.getPlayerById(parseInt(killStat[1]));
    
                if (killer && killed) {
                    // ignore suicide
                    if (killer.clientId !== killed.clientId) {
                        killer.kills++;
                        killer.updateStats();

                        killed.deaths++;
                        killed.updateStats();
                    }
                }
            }
    
            // parse players - AFTER kill stats as client IDs might get reassigned here
            var clientInfoIndex = line.indexOf(LogPattern.ClientUserinfoChanged);
            if (clientInfoIndex > -1) {
                var playerName = line.substr(line.indexOf('n\\') + 2, line.indexOf('\\t\\') - (line.indexOf('n\\') + 2));
                var playerId = parseInt(line.substr(line.indexOf(LogPattern.ClientUserinfoChanged) + LogPattern.ClientUserinfoChanged.length, 1));
    
                newGame.addPlayer(playerName, playerId);
            }
        });
    
        reader.on('close', function (){
            //console.log(log.games[1].players);
            if (outfile) {
                log.exportToJson(outfile);
            }
            done(log);
        });
    }
}