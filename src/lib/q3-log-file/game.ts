import { GameType } from "../enum/game-type.enum";
import { Player } from "./player";

/** A game in the Quake3 logfile.
 * 
 */
export class Game {
    /** The game type [Capture the Flag | Deathmatch | Team Deathmatch] */
    public type: GameType;
    /** Players, who played in the game */
    public players = new Array<Player>();
    /** Chat conversations during the game */
    public chat = new Array<string>();

    constructor() {
        this.type = GameType.Unknown;
    }

    /** Add player to game.
     * @returns undefined | Player instance
     */
    public addPlayer(playerName: string, clientId: number) {
        var newPlayer = new Player();
        newPlayer.name = playerName;
        newPlayer.clientId = clientId;

        for (var player of this.players) {
            // do not add a player if the name already is ingame
            if (player.name === newPlayer.name) {
                return undefined;
            }

            // if clientId is already taken then revoke assignment
            if (player.clientId === newPlayer.clientId) {
                player.clientId = -1;
            }
        }

        this.players.push(newPlayer);
        return newPlayer;
    }

    /** Get player by name */
    public getPlayerByName(name: string) {
        for (var player of this.players) {
            if (player.name === name) {
                return player;
            }
        }

        return undefined;
    }

    /** Get player by id */
    public getPlayerById(id: number) {
        for (var player of this.players) {
            if (player.clientId === id) {
                return player;
            }
        }

        return undefined;
    }
}