export enum LogPattern {
    GameStart = 'InitGame:',
    Chat = 'say: ',
    ClientConnect = 'ClientConnect: ',
    ClientUserinfoChanged = 'ClientUserinfoChanged: ',
    Kill = 'Kill: '
}