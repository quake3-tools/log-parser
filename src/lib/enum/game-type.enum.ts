export enum GameType {
    Unknown = 'Unknown',
    FreeForAll = 'Deathmatch',
    TeamDeathmatch = 'Team Deathmatch',
    CaptureTheFlag = 'Capture the Flag'
}
