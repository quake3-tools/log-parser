import * as fs from 'fs';

import { LogFile } from "./q3-log-file/log-file";
import { Player } from "./q3-log-file/player";

/** Server Stats */
export class ServerStats {
    public log: LogFile;
    public players: Array<Player>;
    public bestPlayer: Player;
    public totalKills: number;
    public totalGames: number;

    constructor(log: LogFile) {
        this.log = log;
        this.players = new Array<Player>();
        this.bestPlayer = new Player();
        this.totalKills = 0;
        this.totalGames = 0;

        this.getPlayerStats();
        this.getTotalKills();
        this.getTotalGames();
    }

    private getTotalGames() {
        this.totalGames = this.log.games.length;
    }

    private getTotalKills() {
        for (var game of this.log.games) {
            for (var player of game.players) {
                this.totalKills = this.totalKills + player.kills
            }
        }
    }

    private getPlayerStats() {
        for (var game of this.log.games) {
            for (var player of game.players) {
                if (this.isPlayerListed(player) === false) {
                    var newPlayer = new Player();
                    newPlayer.clone(player);
                    this.players.push(newPlayer);
                } else {
                    var existingPlayer = this.getPlayerByName(player.name);
                    if (existingPlayer) {
                        existingPlayer.kills  = existingPlayer.kills + player.kills;
                        existingPlayer.deaths = existingPlayer.deaths + player.deaths;
                        existingPlayer.updateStats();
                    }
                }
            }
        }

        this.getBestPlayer();
    }

    private getPlayerByName(playerName: string) {
        for (var curPlayer of this.players) {
            if (curPlayer.name === playerName) {
                return curPlayer;
            }
        }

        return undefined;
    }

    private isPlayerListed(player: Player): boolean {
        var result = false;

        for (var curPlayer of this.players) {
            if (curPlayer.name === player.name) {
                return true;
            }
        }

        return result;
    }

    private getBestPlayer() {
        for (var player of this.players) {
            if (player.kdratio > this.bestPlayer.kdratio) {
                this.bestPlayer.clone(player);
            }
        }
    }

    public exportToJson(file: string, prettyJson?: boolean) {
        var stream = fs.createWriteStream(file);

        var json: string;
        if (prettyJson) {
            json = JSON.stringify(this, undefined, 2);
        } else {
            json = JSON.stringify(this);
        }
        stream.write(json);
        stream.close();
        stream.on('close', function () {
            console.log('File ' + stream.path + ' saved.');
        });
    }
}